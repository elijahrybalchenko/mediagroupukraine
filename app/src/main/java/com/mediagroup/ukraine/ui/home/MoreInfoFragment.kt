package com.mediagroup.ukraine.ui.home

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.mediagroup.ukraine.MainViewModel
import com.mediagroup.ukraine.R
import kotlinx.android.synthetic.main.item_layout.view.iconIV
import kotlinx.android.synthetic.main.item_layout.view.loaderPB
import kotlinx.android.synthetic.main.item_layout.view.nameTV
import kotlinx.android.synthetic.main.more_info_fragment.view.*

class MoreInfoFragment : Fragment() {

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        val root = inflater.inflate(R.layout.more_info_fragment, container, false)
        loadChannelIcon(root)
        root.nameTV.text = viewModel.currentProgram.value?.name
        if (viewModel.currentProgram.value?.description == "") {
            root.descriptionTV.setText(R.string.no_info)
        } else {
            root.descriptionTV.text = viewModel.currentProgram.value?.description
        }

        return root
    }

    private fun loadChannelIcon(itemView: View) {
        Glide.with(itemView.context)
            .load(viewModel.currentProgram.value?.icon)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?, model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    itemView.loaderPB.visibility = View.INVISIBLE
                    itemView.iconIV.visibility = View.VISIBLE
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    itemView.loaderPB.visibility = View.INVISIBLE
                    itemView.iconIV.visibility = View.VISIBLE
                    return false
                }
            })
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(itemView.iconIV)

    }

}