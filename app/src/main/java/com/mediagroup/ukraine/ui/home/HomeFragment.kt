package com.mediagroup.ukraine.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mediagroup.ukraine.MainViewModel
import com.mediagroup.ukraine.R
import com.mediagroup.ukraine.core.adapter.ProgramsAdapter
import com.mediagroup.ukraine.core.listener.InfiniteScrollListener
import com.mediagroup.ukraine.retrofit.model.Program
import kotlinx.android.synthetic.main.home_fragment.*
import java.util.concurrent.atomic.AtomicBoolean


class HomeFragment : Fragment(), InfiniteScrollListener.OnLoadMoreListener,
    ProgramsAdapter.onClickCallback {

    private lateinit var viewModel: MainViewModel
    private lateinit var infiniteScrollListener: InfiniteScrollListener
    private lateinit var adapter: ProgramsAdapter
    private lateinit var update: AtomicBoolean

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        val root = inflater.inflate(R.layout.home_fragment, container, false)

        viewModel.programsListLiveData.observe(viewLifecycleOwner, Observer {
            if (update.compareAndSet(false, true)) {
                val manager = LinearLayoutManager(context)
                infiniteScrollListener =
                    InfiniteScrollListener(
                        manager,
                        this
                    )
                infiniteScrollListener.setLoaded()
                programRV.layoutManager = manager
                programRV.addOnScrollListener(infiniteScrollListener)
                adapter = ProgramsAdapter(it!!, this)
                programRV.adapter = adapter
                progressPB.visibility = View.INVISIBLE
                programRV.visibility = View.VISIBLE
            } else {
                try {
                    adapter.addData(it!!, adapter.removeNull())
                    infiniteScrollListener.setLoaded()
                    programRV.scrollToPosition(it.size)
                } catch (t: Throwable) {
                }

            }

        })


        return root
    }

    override fun onStart() {
        super.onStart()
        progressPB.visibility = View.VISIBLE
        programRV.visibility = View.INVISIBLE
        update = AtomicBoolean(false)
        viewModel.updatePrograms()
        programRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        })
    }

    override fun onLoadDown() {
        adapter.addNullData()
        val list = viewModel.programsListLiveData.value
        if (list != null) {
            viewModel.updatePrograms(list[list.size - 2]!!.id, 1)
        }

    }

    override fun onLoadUp() {
        adapter.addNullData(-1)
        val list = viewModel.programsListLiveData.value
        if (list != null) {
            viewModel.updatePrograms(list[1]!!.id, -1)
        }
    }

    override fun onClick(program: Program) {
        viewModel.updateCurrentProgram(program)
        view?.findNavController()?.navigate(R.id.action_nav_home_to_moreInfoFragment)
    }


}
