package com.mediagroup.ukraine.retrofit.model

import com.google.gson.annotations.SerializedName


data class Program(
    @SerializedName("id") val id: Int,
    @SerializedName("channel_id") val channel_id: Int,
    @SerializedName("channel_name") val channel_name: String,
    @SerializedName("alias") val alias: String,
    @SerializedName("icon") val icon: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("start") val start: String,
    @SerializedName("start_ts") val start_ts: Int,
    @SerializedName("stop") val stop: String,
    @SerializedName("stop_ts") val stop_ts: Int,
    @SerializedName("is_free") val is_free: Int,
    @SerializedName("is_favorite") val is_favorite: Int,
    @SerializedName("under_parental_protect") val under_parental_protect: Int,
    @SerializedName("dvr") val dvr: Int,
    @SerializedName("now") val now: Int,
    @SerializedName("FK_catalog") val fK_catalog: Int,
    @SerializedName("subs") val subs: Subs
)