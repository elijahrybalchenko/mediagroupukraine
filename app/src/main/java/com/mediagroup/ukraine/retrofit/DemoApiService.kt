package com.mediagroup.ukraine.retrofit


import com.mediagroup.ukraine.retrofit.model.DemoResponses
import com.mediagroup.ukraine.utils.Constants.BASE_URL
import com.mediagroup.ukraine.utils.Constants.BORDER_ID
import com.mediagroup.ukraine.utils.Constants.DIRECTION
import com.mediagroup.ukraine.utils.Constants.PATH
import com.mediagroup.ukraine.utils.Constants.SERIAL_NUMBER
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface DemoApiService {
    @GET(PATH)
    fun loadPrograms(
        @Query(SERIAL_NUMBER) serial_number: String,
        @Query(BORDER_ID) borderId: Int,
        @Query(DIRECTION) direction: Int
    ): Call<DemoResponses>

    companion object Factory {
        fun create(): DemoApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(DemoApiService::class.java)
        }
    }
}