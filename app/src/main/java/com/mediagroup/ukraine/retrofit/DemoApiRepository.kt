package com.mediagroup.ukraine.retrofit

import androidx.lifecycle.MutableLiveData
import com.mediagroup.ukraine.retrofit.model.DemoResponses
import com.mediagroup.ukraine.retrofit.model.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object DemoApiRepository {

    val programsLiveData: MutableLiveData<Resource<DemoResponses>> = MutableLiveData()

    fun loadPrograms(
        serial_number: String,
        borderId: Int,
        direction: Int
    ) {
        DemoApiService
            .create()
            .loadPrograms(serial_number, borderId, direction)
            .enqueue(object : Callback<DemoResponses> {
                override fun onFailure(call: Call<DemoResponses>, t: Throwable) {
                    programsLiveData.postValue(Resource.error(t))
                }

                override fun onResponse(
                    call: Call<DemoResponses>,
                    response: Response<DemoResponses>
                ) {
                    if (response.isSuccessful) {
                        programsLiveData.postValue(Resource.success(response.body()))
                    } else {
                        programsLiveData.postValue(Resource.error(NullPointerException()))
                    }

                }
            })
    }
}