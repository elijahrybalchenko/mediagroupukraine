package com.mediagroup.ukraine.retrofit.model

import com.google.gson.annotations.SerializedName


data class Subs(
    @SerializedName("id") val id: Int,
    @SerializedName("price") val price: Int,
    @SerializedName("name") val name: String,
    @SerializedName("subsId") val subsId: Int,
    @SerializedName("type") val type: String
)

