package com.mediagroup.ukraine.retrofit.model

class Resource<T> private constructor(
    val data: T? = null,
    val error: Throwable? = null,
    val type: Type
) {
    enum class Type {
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> success(data: T?) = Resource(data = data, type = Type.SUCCESS)
        fun <T> error(error: Throwable) = Resource<T>(data = null, error = error, type = Type.ERROR)
    }
}