package com.mediagroup.ukraine.retrofit.model

import com.google.gson.annotations.SerializedName

data class DemoResponses(

    @SerializedName("block_id") val block_id: String,
    @SerializedName("block_title") val block_title: String,
    @SerializedName("block_type") val block_type: String,
    @SerializedName("category_name") val category_name: String,
    @SerializedName("first_now_index") val first_now_index: Int,
    @SerializedName("hasMore") val hasMore: Int,
    @SerializedName("items") val items: List<Program?>,
    @SerializedName("items_number") val items_number: Int,
    @SerializedName("offset") val offset: Int,
    @SerializedName("total") val total: Int
)

