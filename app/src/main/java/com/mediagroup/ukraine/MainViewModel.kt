package com.mediagroup.ukraine

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mediagroup.ukraine.retrofit.DemoApiRepository
import com.mediagroup.ukraine.retrofit.model.Program
import com.mediagroup.ukraine.retrofit.model.Resource
import com.mediagroup.ukraine.utils.SerialNumberUtils


class MainViewModel : ViewModel() {
    val programsListLiveData: LiveData<MutableList<Program?>?> =
        Transformations.map(DemoApiRepository.programsLiveData) {
            if (it.data?.hasMore == 0) {

            }
            when (it.type) {
                Resource.Type.SUCCESS -> it.data?.items as MutableList
                Resource.Type.ERROR -> null
            }

        }
    val currentProgram: MutableLiveData<Program> = MutableLiveData()

    fun updatePrograms(borderId: Int = 0, direction: Int = 0) {
        DemoApiRepository.loadPrograms(SerialNumberUtils.getSerialNumber()!!, borderId, direction)

    }

    fun updateCurrentProgram(program: Program) {
        currentProgram.postValue(program)
    }
}