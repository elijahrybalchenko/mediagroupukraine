package com.mediagroup.ukraine.utils

import android.content.Context
import android.content.SharedPreferences
import com.mediagroup.ukraine.App
import java.util.*


class SerialNumberUtils {
    companion object {

        private var uniqueID: String? = null
        private const val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"
        @Synchronized
        fun getSerialNumber(): String? {
            if (uniqueID == null) {
                val sharedPrefs: SharedPreferences = App.context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE
                )
                uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null)
                if (uniqueID == null) {
                    uniqueID = UUID.randomUUID().toString()
                    val editor: SharedPreferences.Editor = sharedPrefs.edit()
                    editor.putString(PREF_UNIQUE_ID, uniqueID)
                    editor.apply()
                }
            }
            return uniqueID
        }

    }
}