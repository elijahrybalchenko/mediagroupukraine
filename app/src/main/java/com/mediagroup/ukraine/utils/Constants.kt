package com.mediagroup.ukraine.utils

object Constants {
    const val BASE_URL = "http://oll.tv/"
    const val PATH = "demo"
    const val SERIAL_NUMBER = "serial_number"
    const val BORDER_ID = "borderId"
    const val DIRECTION = "direction"

}