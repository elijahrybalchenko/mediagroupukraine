package com.mediagroup.ukraine.core.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.mediagroup.ukraine.R
import com.mediagroup.ukraine.retrofit.model.Program
import kotlinx.android.synthetic.main.item_layout.view.*


class ProgramsAdapter(var programList: MutableList<Program?>, var callback: onClickCallback) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_ITEM = 9999
        const val VIEW_TYPE_LOADING = 8888
    }

    interface onClickCallback {
        fun onClick(program: Program)

    }

    inner class ProgramViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(position: Int) {
            initLoader(position)
            itemView.nameTV.text = programList[position]?.name
            itemView.setOnClickListener {
                programList[position]?.let {
                    callback.onClick(it)
                }
            }

        }

        private fun initLoader(position: Int) {
            itemView.loaderPB.visibility = View.VISIBLE
            itemView.iconIV.visibility = View.INVISIBLE

            Glide.with(itemView.context)
                .load(programList[position]?.icon)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?, model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {
                        itemView.loaderPB.visibility = View.INVISIBLE
                        itemView.iconIV.visibility = View.VISIBLE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        itemView.loaderPB.visibility = View.INVISIBLE
                        itemView.iconIV.visibility = View.VISIBLE
                        return false
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemView.iconIV)
        }
    }

    inner class ProgressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(position: Int) {
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (programList[position] != null)
            VIEW_TYPE_ITEM
        else
            VIEW_TYPE_LOADING


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEW_TYPE_ITEM) {
            return ProgramViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_layout,
                    parent,
                    false
                )
            )
        } else {
            return ProgressViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.progress_layout,
                    parent,
                    false
                )
            )
        }

    }

    fun addNullData() {
        programList.add(null)
        notifyItemInserted(programList.size - 1)
    }

    fun addNullData(direction: Int) {
        if (direction == -1) {
            programList.add(0, null)
            notifyItemInserted(0)
        } else {
            programList.add(null)
            notifyItemInserted(programList.size - 1)
        }

    }

    fun removeNull(): Int {
        val index = programList.indexOf(null)
        programList.removeAt(index)
        notifyItemRemoved(index)
        return index
    }

    fun addData(appendList: List<Program?>, index: Int) {
        if (index == 0) {
            programList.addAll(0, appendList)
        } else {
            programList.addAll(appendList)

        }

        notifyDataSetChanged()
    }


    override fun getItemCount(): Int = programList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ProgramViewHolder -> {
                holder.onBind(position)
            }
            is ProgressViewHolder -> {
                holder.onBind(position)
            }
        }
    }
}