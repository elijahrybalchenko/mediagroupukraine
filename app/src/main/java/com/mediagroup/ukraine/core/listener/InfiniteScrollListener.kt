package com.mediagroup.ukraine.core.listener


import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class InfiniteScrollListener(
    private val linearLayoutManager: LinearLayoutManager,
    private val listener: OnLoadMoreListener?
) :
    RecyclerView.OnScrollListener() {
    private var loading = false

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dx == 0 && dy == 0) return
        val totalItemCount = linearLayoutManager.itemCount
        val lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
        val firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition()
        if (!loading && totalItemCount <= lastVisibleItem + VISIBLE_THRESHOLD && totalItemCount != 0) {
            listener?.onLoadDown()
            loading = true
        } else if (!loading && firstVisibleItem - VISIBLE_THRESHOLD <= 0 && totalItemCount != 0 && dy < 0) {
            listener?.onLoadUp()
            loading = true
        }
    }

    fun setLoaded() {
        loading = false
    }

    interface OnLoadMoreListener {
        fun onLoadDown()
        fun onLoadUp()
    }


    companion object {
        private const val VISIBLE_THRESHOLD = 2
    }

}

